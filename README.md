# blockchain
#### 项目介绍
区块链技术  
本项目内容为区块链技术教程程序样例。如您觉得该项目对您有用，欢迎点击右上方的Star按钮，给予支持！！
* simple-demo 200行代码简单示例
* p2p 对等网络简单实现
* proof-of-work 工作量证明
* proof-of-stake 股权证明
* eth 以太坊及其开发环境
* solidity 智能合约案列
* eth-dapp 以太坊智能合约web应用
* btc 比特币


#### 入门
- [理解区块链](http://blog.csdn.net/csolo/article/details/52858236) 区块链关键技术要点讲解
- [共识算法与如何解决拜占庭将军问题](https://charlesliuyx.github.io/2018/03/03/%E3%80%90%E5%8C%BA%E5%9D%97%E9%93%BE%E3%80%91%E5%A6%82%E4%BD%95%E8%A7%A3%E5%86%B3%E6%8B%9C%E5%8D%A0%E5%BA%AD%E5%B0%86%E5%86%9B%E9%97%AE%E9%A2%98/)

#### 公链项目
- [BTC](https://github.com/bitcoin/bitcoin) 比特币
- [ETH](https://github.com/ethereum/go-ethereum)  以太坊
- [EOS](https://github.com/EOSIO/eos) EOS
- [LITECOIN](https://github.com/litecoin-project/litecoin) 莱特币
- [QTUM](https://github.com/qtumproject/qtum) 量子链
- [BYTOM(BTM)](https://github.com/Bytom/bytom) 比原链
- [NEO](https://github.com/neo-project/neo) 小蚁
- [ZILLIGA](https://github.com/Zilliqa/Zilliqa) 
- [CARDANO(ADA)](https://github.com/input-output-hk/cardano-sl) 艾达币
- [METAVERSE](https://github.com/mvs-org/metaverse) 元界
- [NEBULAS](https://github.com/nebulasio/go-nebulas) 星云

#####  区块链体系
![](architecture.jpg)
